package com.company;

import coordinates.Circle;
import coordinates.PointList;

import java.util.Scanner;

/*
        Пользователь вводит координаты точек
        Затем программа спрашивает координаты центра и радиус окружности
        Вывести только те точки, которые лежат в окружности
 */

public class Main {
    public static final Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {

        taskInterface();
    }

    private static void choices(PointList pointList) {

        System.out.println("| Желаете добавить еще (y/n)");
        String choice = scan.next();
        switch (choice) {
            case "y":
                System.out.println("Ваш выбор: " + choice);
                System.out.print("|x: ");
                double x = scan.nextInt();
                System.out.print("|y: ");
                double y = scan.nextInt();
                pointList.add(x, y);
                System.out.println(pointList.size());
                choices(pointList);
                break;
            case "n":
                System.out.println("Ваш выбор: " + choice);
                break;
            default:
                System.out.println("Вы ввели неверную цифру");
                choices(pointList);
                break;
        }
    }

    private static void taskInterface() {
        PointList pointList = new PointList();
        System.out.println("| введите координаты точки");
        System.out.print("|x: ");
        double x = scan.nextInt();
        System.out.print("|y: ");
        double y = scan.nextInt();
        pointList.add(x, y);
        pointList.size();
        choices(pointList);
        System.out.println("Введите координаты центра и радиуса окружности: ");
        System.out.print("center x: ");
        double centerX = scan.nextDouble();
        System.out.print("center y: ");
        double centerY = scan.nextDouble();
        System.out.print("radius: ");
        double radius = scan.nextDouble();
        Circle circle = new Circle(centerX, centerY, radius);
        circle.checkPoints(pointList);
    }


}
